#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#define SUBHOOK_STATIC
#include <subhook.c>

#if defined(MOTH_RELEASE)
#   define mod_log(...) (0)
#   define mod_backtrace()
#else

#include <execinfo.h>
#include <stdarg.h>

static int
mod_log (char const* fmt, ...)
{
   va_list va;
   int size;

   fprintf(stderr, "2 Modified: ");
   va_start(va, fmt);
   size = vfprintf(stderr, fmt, va);
   va_end(va);

   return size;
}

#define mod_backtrace() \
   { \
      void*  frames[100]; \
      char** frameSyms; \
      int    frameCount; \
      \
      frameCount = backtrace(frames, 100); \
      frameSyms  = backtrace_symbols(frames, frameCount); \
      mod_log("%s call depth is %i frames:\n", __func__, frameCount); \
      \
      if (frameSyms) \
      { \
         for (int idx = 0; idx < frameCount; ++idx) \
         { \
            mod_log("%-3i : %s\n", idx, frameSyms[idx]); \
         } \
         free(frameSyms); \
      } \
      else \
      { \
         mod_log("\tError: could not generate symbolic trace\n"); \
      } \
   }
#endif

/**
 * Registration State Structure
 */

struct RRegister
{
   uint8_t __padding_00B[0x0B]; // 0x00 .. 0x0B
   int32_t  isSingleUser;       // 4 Bytes @ +0x0C
   uint8_t __padding_020[0x08]; // 0x10 .. 0x18
   char*    __field_18;         // 8 Bytes @ +0x18
   uint32_t date_0;             // 4 Bytes @ +0x20
   uint32_t date_1;             // 4 Bytes @ +0x24
   uint32_t __field_28;         // 4 Bytes @ +0x28
   uint32_t version;            // 4 Bytes @ +0x2C
   uint32_t users;              // 4 Bytes @ +0x30
   uint32_t usesLeft;           // 4 Bytes @ +0x34
   uint32_t __field_38;         // 4 Bytes @ +0x38
   uint32_t isInvalid;          // 4 Bytes @ +0x3C
   uint32_t __field_40;         // 4 Bytes @ +0x40
   uint32_t daysLeft;           // 4 Bytes @ +0x44

};

// Global registration data structure; Version 8.0 64 .bss:0000000000C34D38; Exported
extern struct RRegister gRegister;

/**
 * Status Check
 *
 * Other Return Values:
 *
 * Bad Return Values:
 * - 47     (System Clock Error)
 * - 113    (Evaluation, Time-based)
 * - 249    (Evaluation, Launch-based)
 * - 237    (Evaluation Expired)
 * - 275    (Expired)
 * - 375    (Evaluation Period Expired)
 * - 524    (Upgrade Required)
 *
 * Good Return Values:
 * - 219    (Registered)
 */

static uint64_t rep_Register$CheckStatus(int64_t stk1, int64_t stk2)
{
   return 219;
}

static uint64_t off_Register$CheckStatus = 0x562630;
static subhook_t sh_Register$CheckStatus;

/**
 * License Type Checks
 */

static uint8_t
rep_Register$GetIsSiteLicense(/* rdi */)
{
   return 1;
}

static uint64_t off_Register$GetIsSiteLicense = 0x562780;
static subhook_t sh_Register$GetIsSiteLicense;

static void __attribute__ ((constructor))
moth_init (void)
{
   unsetenv("LD_PRELOAD"); // Prevent moth from breaking children

   sh_Register$CheckStatus = subhook_new((void*) off_Register$CheckStatus, (void*) &rep_Register$CheckStatus, SUBHOOK_OPTION_64BIT_OFFSET);
   sh_Register$GetIsSiteLicense = subhook_new((void*) off_Register$GetIsSiteLicense, (void*) &rep_Register$GetIsSiteLicense, SUBHOOK_OPTION_64BIT_OFFSET);
   subhook_install(sh_Register$CheckStatus);
   subhook_install(sh_Register$GetIsSiteLicense);
}
