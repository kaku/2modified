# vim: noexpandtab tabstop=3 shiftwidth=3 :
CC			= gcc
CFLAGS	= -Isubhook -D_GNU_SOURCE -fPIC
STRIP	  	= sstrip

.DEFAULT_GOAL := bin/2modified.so

bin/:
	mkdir bin

bin/2modified.so: src/2modified.c bin/
	$(CC) $(CFLAGS) src/2modified.c -ldl -shared -o bin/2modified.so

release: CFLAGS += -DMOTH_RELEASE -g0
release: bin/2modified.so
	$(STRIP) bin/2modified.so

clean:
	-rm bin/2modified.so
